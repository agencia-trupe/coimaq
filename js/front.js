$('document').ready( function(){

	$('#slideshow #animate').cycle({
		pager : $('#slideshow #navegacao')
	});

	var altura_coluna, altura_minima = 0;
	$('#chamadas-home-container .coluna').each( function(){
		altura_coluna = parseInt($(this).css('height'));
		if(altura_coluna > altura_minima)
			altura_minima = altura_coluna;
	});
	$('#chamadas-home-container .coluna').css('height', altura_minima);

	$('.main-importacao .coluna-lateral ul a').click(function(e){

		e.preventDefault();

		var contexto = $('.main-importacao .coluna-lateral ul');
		var destino = $(this).attr('href').split('/').pop();
		var visivel = $('.coluna-direita .box:visible');

		if($('.ativo', contexto).length)
			$('.ativo', contexto).removeClass('ativo');
		
		$(this).addClass('ativo');

		if(visivel.length){
			if(!visivel.hasClass(destino)){
				visivel.fadeOut('normal', function(){
					$('.coluna-direita .box.'+destino).fadeIn('normal');
				});
			}
		}else{
			$('.coluna-direita .box.'+destino).fadeIn('normal');
		}		

	});

	$('#form-newsletter').submit( function(e){
		e.preventDefault();

		$.post(BASE+'index.php/home/cadastroNews', { nome : $('#newsletter-nome').val(), email : $('#newsletter-email').val() }, function(){
			$('#form-newsletter').addClass('hid');
			setTimeout( function(){
				$('#mensagem-newsletter').removeClass('hid');
				$('#newsletter-nome').val('');
				$('#newsletter-email').val('');	
					setTimeout(function(){
						$('#mensagem-newsletter').addClass('hid');
						setTimeout( function(){
							$('#form-newsletter').removeClass('hid');	
						}, 400);
					}, 10000);	
			}, 400);
		});
	});

});