<?php
/**************************
    Prefixo : MN
 	ITENS DO MENU
***************************/
$lang['MN Home'] = 'HOME';
$lang['MN Empresa'] = 'EMPRESA';
$lang['MN Serviços'] = 'SERVIÇOS';
$lang['MN Importação'] = 'IMPORTAÇÃO';
$lang['MN Contato'] = 'CONTATO';


/**************************
    Prefixo : ATIT    	 
 	TÍTULOS DE LINKS
***************************/
$lang['ATIT Página Inicial'] = 'Página Inicial';
$lang['ATIT Empresa'] = 'Empresa';
$lang['ATIT Serviços'] = 'Serviços';
$lang['ATIT Importação'] = 'Importação';
$lang['ATIT Contato'] = 'Contato';


/**************************
    Prefixo : RDP
 	TEXTOS DO RODAPÉ
***************************/
$lang['RDP ENTRE EM CONTATO'] = 'ENTRE EM CONTATO';
$lang['RDP Todos os direitos reservados'] = 'Todos os direitos reservados';


/**************************
    Prefixo : HOME
 	TEXTOS DA HOME
***************************/
$lang['HOME NOSSO PROCESSO'] = "NOSSO PROCESSO";

$lang['HOME Texto Slide 1'] = 'As pesquisas podem ser realizadas em qualquer região da China ou países vizinhos. Buscamos por produtos e fornecedores que atendam as necessidades de nossos clientes, tanto no que se refere às condições comerciais quanto às qualidades exigidas.';
$lang['HOME Texto Slide 2'] = 'Com equipe especializada e fluência nos idiomas locais, a <strong>COIMAQ</strong> poderá acompanhar todo o processo produtivo, desde as inspeções de matéria-prima até as inspeções no momento do carregamento.';
$lang['HOME Texto Slide 3'] = 'A <strong>COIMAQ</strong> estrutura operações de importação e exportação de diversos produtos dos mais variados segmentos de negócio. Oferecemos integração logística, consolidação de cargas e gestão financeira no processo de comércio exterior, administrando a carteira de pedidos com os fornecedores.';

$lang['HOME FERIAS E EVENTOS INTERNACIONAIS'] = 'FEIRAS E EVENTOS INTERNACIONAIS';
$lang['HOME Chamada Viagens'] = "Realizamos a logística de suas<br>viagens, feiras e eventos na China.";
$lang['HOME PROGRAME SUA VIAGEM'] = "PROGRAME SUA VIAGEM";

$lang['HOME Texto Faixa'] = "Conheça alguns dos principais serviços que podemos fazer pela sua empresa no mercado internacional";

$lang['HOME Chamada Inferior 1 Titulo'] = "Pesquisa de Produtos e Fornecedores";
$lang['HOME Chamada Inferior 1 Texto'] = "As pesquisas podem ser realizadas em qualquer região da China ou países vizinhos. Buscamos por produtos e fornecedores que atendam as necessidades de nossos clientes, tanto no que se refere às condições comerciais quanto às qualidades exigidas.";
$lang['HOME Chamada Inferior 2 Titulo'] = "Controle de Qualidade";
$lang['HOME Chamada Inferior 2 Texto'] = "Com equipe especializada em inspeções e auditorias, a <strong>COIMAQ</strong> poderá acompanhar todo o processo produtivo, realizando inspeções de matéria-prima, produção e embarque, até a entrega da mercadoria no seu destino final, garantindo ao importador a segurança e confiabilidade desejada.";
$lang['HOME Chamada Inferior 3 Titulo'] = "Desenvolvimento de Produtos";
$lang['HOME Chamada Inferior 3 Texto'] = "Assessoramos nossos clientes que desejam terceirizar o processo de fabricação e desenvolver novos produtos no mercado do chinês. O projeto inclui o desenvolvimento do produto, embalagens, manuais, conteúdo e demais necessidades. O produto poderá ser customizado para atender diversas exigências e normas de cada segmento.";
$lang['HOME Texto Newsletter'] = "Cadastre-se para receber novidades, informativos e convites de eventos";
$lang['Retorno Newsletter'] = "Cadastro efetuado com sucesso!";

/**************************
    Prefixo : SUB
 	TEXTOS DO SUBMENU
***************************/
$lang['SUB Importação de Produtos da China'] = "Importação de Produtos da China";
$lang['SUB Pesquisa de Produtos e Fornecedores'] = "Pesquisa de Produtos e Fornecedores";
$lang['SUB Desenvolvimento de Produtos'] = "Desenvolvimento de Produtos";
$lang['SUB Inspeção de Qualidade'] = "Inspeção de Qualidade";
$lang['SUB Logística Internacional'] = "Logística Internacional";


/**************************
    Prefixo : --
 	TEXTOS COMUNS
***************************/
$lang['saiba mais'] = "saiba mais";
$lang['Nome'] = "Nome";
$lang['E-mail'] = "E-mail";
$lang['ENVIAR'] = "ENVIAR";
$lang['BRASIL - SÃO PAULO'] = 'BRASIL - SÃO PAULO';
$lang['CHINA - XANGAI'] = 'CHINA - XANGAI';

/**************************
    Prefixo : EMPRESA
 	TEXTOS DA SEÇÃO EMPRESA
***************************/
$lang['EMPRESA Titulo'] = "Comércio Internacional com profissionalismo e transparência";
$lang['EMPRESA CONHEÇA NOSSOS SERVIÇOS'] = "CONHEÇA NOSSOS SERVIÇOS";

$lang['EMPRESA Texto'] = <<<STR
<p>
A <strong>COIMAQ</strong> é uma empresa inovadora criada para atender empresas que desejam internacionalizar seus departamentos de compras e realizar negócios no mercado chinês com solidez, qualidade e transparência. Para isso, contamos com uma equipe qualificada de profissionais, segmentada por linhas de negócio e fluência nos idiomas locais, proporcionando melhores negociações e satisfação total para os nossos clientes.
</p>
<p>
Com <span class='destaque'>sedes próprias no Brasil, China e Argentina</span>, a <strong>COIMAQ</strong> conta com uma estrutura sólida para prospectar, qualificar e garantir a entrega do que é produzido e vendido para o os nossos clientes. Garantimos a qualidade dos produtos e a satisfação do cliente, pois toda a cadeia é gerenciada por profissionais especializados que acompanham o processo desde a compra, produção até a entrega no destino final. 
</p>
<h3>NOSSA VISÃO</h3>
<p>
"Nós da <strong>COIMAQ</strong>, sonhamos com um futuro onde os produtos serão fabricados sem defeitos, onde empresas não empregarão pessoas para realizar trabalhos repetitivos, perigosos ou insalubres e onde a natureza será respeitada. Auxiliamos nossos clientes a construir esse futuro, tornando-os mais eficientes, mais inovadores e criativos".
</p>
<h3>NOSSA MISSÃO</h3>
<p>
Proporcionar ao mercado brasileiro produtos e serviços inovadores com a mais alta qualidade e  que auxiliem nossos clientes a ganhar competitividade com qualidade.
</p>
<h3>VALORIZAÇÃO DOS PARCEIROS</h3>
<p>
Através da ética e transparência  a COIMAQ valoriza os que fazem parte do nosso sucesso, pois por trás de toda relação comercial existem pessoas.
</p>
STR;


/**************************
    Prefixo : SERVIÇOS
 	TEXTOS DA SEÇÃO SERVIÇOS
***************************/
$lang['SERVIÇOS Titulo']['importacao'] = "O que a COIMAQ pode fazer pela sua empresa no mercado internacional?";
$lang['SERVIÇOS Texto']['importacao'] = <<<STR
<img src='_imgs/internas/servicos_sub1.jpg'>
<p>
A China atualmente é considerada a fábrica do mundo e a COIMAQ poderá incluir sua empresa no mercado internacional, mercado este que se torna cada vez mais competitivo e necessário. Com estrutura própria, a COIMAQ poderá realizar um BPO (Business Process Outsourcing ou Terceirização de Processos Empresariais) dos processos de compras, projetos e produção. De acordo com especialistas em planejamento corporativo, a terceirização de processos empresariais leva a uma mudança organizacional significativa e positiva, visto que a co-responsabilidade se converte em um elemento fundamental e de fácil administração.
</p>
<p>
Este processo de terceirização permite que nossos clientes atuem com um maior foco no core business da empresa, alavancando vendas, reduzindo custos diretos e indiretos, além de aumentar a competitividade no mercado em questão, seja por melhores preços e/ou aumento da qualidade.
</p>
<p>
No BPO da COIMAQ, evitamos possíveis riscos que a terceirização pode provocar nas organizações, colaborando com nossos clientes em três elementos-chave: transparência, confiança e escalabilidade.  Para a COIMAQ, a confiança implica em estabelecer mecanismos para flexibilizar o serviço, compartilhar riscos, benefícios e garantir a entrega do trabalho proposto.
</p>
<p>
O termo “incluir sua empresa no mercado internacional” significa explorar e aproveitar a capacidade produtiva em escala, característica predominante na China, para reduzir custos e riscos na operação. Outro ponto importante é que a Coimaq proporciona aos clientes, acesso a inovações, novas tecnologias e criações de diversas empresas ao redor mundo. Muitos fornecedores desenvolvem produtos inovadores e a COIMAQ poderá compartilhar todo esse rico portfólio com a sua empresa.
</p>
STR;


$lang['SERVIÇOS Titulo']['pesquisa'] = "O que a COIMAQ pode fazer pela sua empresa no mercado internacional?";
$lang['SERVIÇOS Texto']['pesquisa'] = <<<STR
<img src='_imgs/internas/servicos_sub2.jpg'>
<p>
As pesquisas podem ser realizadas em qualquer região na China ou países vizinhos cujas condições comerciais e de produto atendam as expectativas dos nossos clientes. A COIMAQ realiza busca em diversas regiões e em diversos fornecedores, a fim de garantir aos nossos clientes opções para tomadas de decisão e melhores preços, sem ficar refém de um único fornecedor.
</p>
<p>
O processo de seleção e pesquisa de fornecedores considera parâmetros definidos no inicio de cada pesquisa. O resultado será um documento com os possíveis fornecedores devidamente qualificados e pontuados, incluindo fotos, certificados e informações acessórias para que cada cliente possa selecionar o melhor produto e o melhor fornecedor.
</p>
<h3>
	O processo pode incluir os seguintes passos:
</h3>
<ul>
	<li>Pesquisa e avaliação dos fornecedores;</li>
	<li>Auditorias de fábrica;</li>
	<li>Avaliações e envio de amostras dos produtos;</li>
	<li>Avaliação laboratorial;</li>
	<li>Obtenção dos certificados específicos.</li>
</ul>
STR;


$lang['SERVIÇOS Titulo']['desenvolvimento'] = "Deseja desenvolver um produto com sua marca? Seu produto possui necessidades específicas?";
$lang['SERVIÇOS Texto']['desenvolvimento'] = <<<STR
<img src='_imgs/internas/servicos_sub3.jpg'>
<p>
A COIMAQ assessora empresas que desejam terceirizar o processo de fabricação e desenvolvimento de produtos, projetos e marcas na China. Caso não haja similar, a COIMAQ pode desenvolver o projeto, desenhos, embalagens, cronograma e todos os passos que antecedem a seleção de fornecedores para produção.
</p>
<p>
Quando o cliente já possui o projeto aprovado, formatamos uma proposta que inicia no entendimento do produto, analise dos pré-requisitos, relatórios de qualidade, homologação, negociação e entrega.
</p>
<h3>Processo completo</h3>
<img src='_imgs/internas/servicos_processo.png'>
<ol>
	<li><span class='destaque'>1</span>. Entendimento e análise dos produtos;</li> 
	<li><span class='destaque'>2</span>. Qualificação de fornecedores;</li>
	<li><span class='destaque'>3</span>. Auditoria da fábrica;</li>
	<li><span class='destaque'>4</span>. Produção e aprovação das amostras;</li>
	<li><span class='destaque'>5</span>. Contrato de fornecimento;</li>
	<li><span class='destaque'>6</span>. Gestão da carteira de pedidos;</li>
	<li><span class='destaque'>7</span>. Inspeções de qualidade;</li>
	<li><span class='destaque'>8</span>. Logística geral;</li>
</ol>
<p>
De acordo com as necessidades de cada empresa o projeto poderá sofrer adequações. O processo também poderá incluir o desenvolvimento e fabricação de embalagens customizadas.
</p>
STR;


$lang['SERVIÇOS Titulo']['qualidade'] = "O que a COIMAQ pode fazer pela sua empresa no mercado internacional?";
$lang['SERVIÇOS Texto']['qualidade'] = <<<STR
<img src='_imgs/internas/servicos_sub4.jpg'>
<img src='_imgs/internas/passos123.jpg' style='margin-top:0;'>
<h3>
	Inspeção inicial da produção
</h3>
<p>
O objetivo dessa inspeção é prevenir e antecipar a detecção possíveis problemas e falhas, além de recomendar especificações para que o produto contemple todas as qualidades e funcionalidades necessárias. Nesta inspeção verificam-se os seguintes aspectos:
</p>
<ul>
	<li>Linhas de produção e capacidade produtiva do fornecedor;</li>
	<li>Equipamentos e tecnologia da fábrica;</li>
	<li>Matérias-primas, principais componentes e acessórios;</li>
	<li>Produtos semiacabados e insumos;</li>
	<li>Estoque e local de armazenagem;</li>
	<li>Embalagens.</li>
</ul>
<h3>
	Inspeção durante a produção
</h3>
<p>
	Essa inspeção é realizada quando pelo menos 20% ou 30% da produção estiver iniciada. Cada etapa do processo é analisada, incluindo a inspeção visual dos produtos. 
</p>
<p>
Nesse momento os produtos acabados serão comparados com as amostras previamente aprovadas e novas amostras poderão ser enviadas para aprovação final do cliente.
</p>
<h3>
	Inspeção pré-embarque
</h3>
<p>
Essa inspeção é realizada antes do embarque da mercadoria e inclui inspeções por amostragem ou unitariamente dependendo do tipo de produto. Serão avaliadas as embalagens e o container, com objetivo de não comprometer o produto em transito.
</p>
<p>
	Nesta inspeção verifica-se principalmente:
</p>
<ul>
	<li>Documentação;</li>
	<li>Quantidades;</li>
	<li>Dimensões e peso; </li>
	<li>Embalagem e empacotamento, para prevenir avarias durante o transporte; </li>
	<li>Testes de segurança e qualidade;</li>
	<li>Lacres.</li>
<ul>
<p>
Para determinados produtos, (como por exemplo, matérias primas e produtos químicos) a COIMAQ poderá realizar testes laboratoriais específicos. Os laudos serão enviados aos nossos clientes no idioma de preferência.
</p>
STR;


$lang['SERVIÇOS Titulo']['logistica'] = "O que a COIMAQ pode fazer pela sua empresa no mercado internacional?";
$lang['SERVIÇOS Texto']['logistica'] = <<<STR
<img src='_imgs/internas/servicos_sub5.jpg'>
<p>
A COIMAQ oferece soluções logísticas completas, incluindo coordenação de transporte internacional em qualquer modal (aéreo, marítimo, rodoviário e ferroviário) e outros serviços adicionais como seguro de transportes de mercadorias, consolidação, armazenagem e distribuição. 
</p>
<p>	
Com uma ampla rede de parceiros logísticos espalhados ao redor do mundo e funcionários qualificados para efetuar operações de logística complexas, a COIMAQ busca sempre pela excelência na gestão da cadeia de suprimentos de nossos clientes. A excelência na gestão desse processo reduzirá o lead time de entrega e, consequentemente, os custos de estoque e de transporte.
</p>
<h3>
	Serviços
</h3>
<ul>
	<li>Transporte Aéreo;</li>
	<li>Transporte Marítimo;</li>
	<li>Serviços de NVOCC;</li>
	<li>Embarques FCL;</li>
	<li>Consolidação de cargas / LCL;</li>
	<li>Serviços “door to door”;</li>
	<li>Fretamentos, projetos, cargas RO RO.</li>
</ul>
STR;


/**************************
    Prefixo : IMPORTAÇÃO
 	TEXTOS DA SEÇÃO IMPORTAÇÃO
***************************/
$lang['IMPORTAÇÃO Titulo'] = "Comércio Internacional com profissionalismo e transparência";
$lang['IMPORTAÇÃO Já importa?'] = "SUA EMPRESA JÁ IMPORTA?";
$lang['IMPORTAÇÃO Sim'] = "Sim";
$lang['IMPORTAÇÃO Não'] = "Não";
$lang['IMPORTAÇÃO Importante Saber Titulo'] = "IMPORTANTE SABER!";
$lang['IMPORTAÇÃO Importante Saber Texto'] = <<<STR
<p>
A China conta hoje com um grande potencial fabril e com preços muito agressivos frente aos demais países. Isso se dá, principalmente, pelo incentivo a produção dentro do país (impulsionado pela grande quantidade de mão de obra) e as baixas cargas tributárias as quais as indústrias são submetidas.
</p>	
<p>
Com aproximadamente 9,6 milhões de quilômetros quadrados e com uma população de 1,339 bilhões de habitantes, a República Popular da China, é hoje, sem dúvida, a fábrica do mundo. Com um PIB Nominal de aproximadamente 7,3 trilhões de dólares (dados de 2011), fazem da China um país em continua ascensão.
</p>
STR;
$lang['IMPORTAÇÃO Texto não'] = <<<STR
<p>
Nossa proposta é desenvolver uma estrutura comercial na China, gerindo os processos de compras de mercadorias (“procurement”), produção, controle de qualidade e logística internacional. 
</p>
<p>
Através de uma estrutura sólida, a <strong>COIMAQ</strong> poderá auxiliar sua empresa a reduzir substancialmente os custos na aquisição de produtos no mercado internacional, independente do segmento, visando sempre a segurança e melhora contínua do processo.
</p>
<p>
Os produtos serão exportados pela <strong>COIMAQ</strong> diretamente para o importador, evitando bitributações, eliminando intermediários e gastos adicionais.
</p>
STR;
$lang['IMPORTAÇÃO Texto sim'] = <<<STR
<p>
Realizamos todos os serviços relacionados ao processo de comércio exterior, desde a pesquisa de produtos e fornecedores, desenvolvimento de projetos especiais, inspeções de qualidade, consolidação de carga, exportação, frete local e logística internacional.
</p>
STR;


/**************************
    Prefixo : CONTATO
 	TEXTOS DA SEÇÃO CONTATO
***************************/
$lang['CONTATO Título'] = "Em caso de dúvidas, sugestões e demais informações, entre em contato";
$lang['CONTATO Titulo Formulario'] = "Envie-nos uma mensagem através do formulário";
$lang['CONTATO Nome'] = "Nome";
$lang['CONTATO Nome da Empresa'] = "Nome da Empresa";
$lang['CONTATO E-mail'] = "E-mail";
$lang['CONTATO Telefone'] = "Telefone";
$lang['CONTATO Mensagem'] = "Mensagem";
$lang['CONTATO ENVIAR'] = "ENVIAR";
$lang['CONTATO Resposta de Contato'] = "Obrigado por entrar em contato.<br>Responderemos assim que possível.";

$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";

?>