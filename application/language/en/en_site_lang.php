<?php
/**************************
    Prefixo : MN
 	ITENS DO MENU
***************************/
$lang['MN Home'] = 'HOME';
$lang['MN Empresa'] = 'COMPANY';
$lang['MN Serviços'] = 'SERVICES';
$lang['MN Importação'] = 'IMPORTING';
$lang['MN Contato'] = 'CONTACTS';


/**************************
    Prefixo : ATIT    	 
 	TÍTULOS DE LINKS
***************************/
$lang['ATIT Página Inicial'] = 'Start Page';
$lang['ATIT Empresa'] = 'Company';
$lang['ATIT Serviços'] = 'Services';
$lang['ATIT Importação'] = 'Importing';
$lang['ATIT Contato'] = 'Contacts';


/**************************
    Prefixo : RDP
 	TEXTOS DO RODAPÉ
***************************/
$lang['RDP ENTRE EM CONTATO'] = 'CONTACT US!';
$lang['RDP Todos os direitos reservados'] = 'All rights reserved';

/**************************
    Prefixo : HOME
 	TEXTOS DA HOME
***************************/
$lang['HOME NOSSO PROCESSO'] = "OUR PROCESS";

$lang['HOME Texto Slide 1'] = 'Searches/sourcing can be performed in any region of China or neighboring countries. We search for products and suppliers that meet the needs of our customers, both in respect to commercial terms as to the standard of quality required.';
$lang['HOME Texto Slide 2'] = 'With specialized team in inspections and audits, <strong>COIMAQ</strong> can monitor the entire production process, conducting inspections of raw materials, production and shipment to delivery of the goods at their final destination, providing a guarantee to the importer, ensuring the safety and reliability desired. Learn more.';
$lang['HOME Texto Slide 3'] = '<strong>COIMAQ</strong> structure offers the opportunity of import and export operations of various products from many different business segments. We offer integrated logistics, cargo consolidation and financial management in the process of foreign trade, managing the order schedule and complete process together with suppliers.';

$lang['HOME FERIAS E EVENTOS INTERNACIONAIS'] = 'INTERNATIONAL FAIRS AND EVENTS';
$lang['HOME Chamada Viagens'] = "COIMAQ can assist your company in planning, translation, negotiation and all travel logistics for various exhibitions in China.";
$lang['HOME PROGRAME SUA VIAGEM'] = "SCHEDULE YOUR TRIP";

$lang['HOME Texto Faixa'] = "What COIMAQ can do for your business in the international market?";

$lang['HOME Chamada Inferior 1 Titulo'] = "Search for Products and Suppliers";
$lang['HOME Chamada Inferior 1 Texto'] = "Searches/sourcing can be performed in any region of China or neighboring countries. We search for products and suppliers that meet the needs of our customers, both in respect to commercial terms as to the standard of quality required.";
$lang['HOME Chamada Inferior 2 Titulo'] = "Quality Control";
$lang['HOME Chamada Inferior 2 Texto'] = "With specialized team in inspections and audits, COIMAQ can monitor the entire production process, conducting inspections of raw materials, production and shipment to delivery of the goods at their final destination, providing a guarantee to the importer, ensuring the safety and reliability desired. Learn more.";
$lang['HOME Chamada Inferior 3 Titulo'] = "Product Development";
$lang['HOME Chamada Inferior 3 Texto'] = "We advise our clients who wish to outsource the manufacturing process and development of new products in the Chinese market. The project includes the development of the product, customized packaging, manuals, content and other needs. The product can be customized to meet different requirements and standards of each segment. Learn more.";
$lang['HOME Texto Newsletter'] = "Cadastre-se para receber novidades, informativos e convites de eventos"; /**/
$lang['Retorno Newsletter'] = "Cadastro efetuado com sucesso!"; /**/

/**************************
    Prefixo : SUB
 	TEXTOS DO SUBMENU
***************************/
$lang['SUB Importação de Produtos da China'] = "Importing Products from China";
$lang['SUB Pesquisa de Produtos e Fornecedores'] = "Search for Products and Suppliers";
$lang['SUB Desenvolvimento de Produtos'] = "Project Development";
$lang['SUB Inspeção de Qualidade'] = "Quality Inspection";
$lang['SUB Logística Internacional'] = "International Logistics";


/**************************
    Prefixo : --
 	TEXTOS COMUNS
***************************/
$lang['saiba mais'] = "learn more";
$lang['Nome'] = "Name";
$lang['E-mail'] = "E-mail";
$lang['ENVIAR'] = "SEND";
$lang['BRASIL - SÃO PAULO'] = 'BRASIL - SÃO PAULO';
$lang['CHINA - XANGAI'] = 'CHINA - XANGAI';

/**************************
    Prefixo : EMPRESA
 	TEXTOS DA SEÇÃO EMPRESA
***************************/
$lang['EMPRESA Titulo'] = "International Trade with professionalism and transparency";
$lang['EMPRESA CONHEÇA NOSSOS SERVIÇOS'] = "CONHEÇA NOSSOS SERVIÇOS";

$lang['EMPRESA Texto'] = <<<STR
<p>
The <strong>COIMAQ</strong> is an innovative company created to meet Latin American and Asian companies who want to internationalize theirs purchasing departments and conduct business in the market with reliability, quality and transparency.
</p>
<p>
And for that, we are counting with a qualified team of professionals, segmented by business lines and fluency in local languages, providing a greater understanding and better negotiations.
</p>
<p>
With <span class='destaque'>headquartered in China - Shanghai, Brazil - São Paulo and Argentina - Buenos Aires</span>, <strong>COIMAQ</strong> has a solid structure to prospect, qualify and ensure the delivery of what is produced and sold to the Latin American market. We guarantee product quality, because all chain has been managed by professionals who monitor the process from purchase and production, till delivery at final destination.
</p>
<h3>OUR VISION</h3>
<p>
At <strong>COIMAQ</strong>, we dream of a future where the products will be manufactured defects-free, where companies do not employ people to perform repetitive, dangerous or unhealthy tasks, and where the nature will be respected. We help our clients to build this future for making them more efficient, more innovative and creative. We work for the good of society, a better life and a better world for all. Following this philosophy, we aim to be one of the largest trade and Imports Company from Brazil.
</p>
<h3>OUR MISSION</h3>
<p>
Provide to the domestic market, products and services of the highest quality and innovation that help our clients to increase the competitiveness in their business.
</p>
<h3>VALUATION OF PARTNERS</h3>
<p>
Through ethics and transparency, COIMAQ values the ones that are part of our success and assumes that behind every business relationship there is people’s nature.
</p>
STR;


/**************************
    Prefixo : SERVIÇOS
 	TEXTOS DA SEÇÃO SERVIÇOS
***************************/
$lang['SERVIÇOS Titulo']['importacao'] = "What COIMAQ can do for your business in the international market?";
$lang['SERVIÇOS Texto']['importacao'] = <<<STR
<img src='_imgs/internas/servicos_sub1.jpg'>
<p>
China currently is considered the world's factory and COIMAQ may include your company in the international market, which market is becoming increasingly competitive and necessary. With proper structure, COIMAQ can perform a BPO (Business Process Outsourcing and Business Process Outsourcing) of purchasing processes, projects and production. According to corporate planning experts, business process outsourcing leads to a significant and positive organizational change, since the co-responsibility becomes a fundamental element and easy to administer.
</p>
<p>
This outsourcing process allows our clients to act with a greater focus on core business, leveraging sales, reducing direct and indirect costs, and increase competitiveness in the market in question, either for better prices and / or higher quality.
</p>
<p>
In COIMAQ’s BPO, we aim to avoid potential risks that outsourcing can have on organizations, collaborating with our clients in three key elements: transparency, reliability and scalability. For COIMAQ, trust involves establishing mechanisms to ease the service, share risks, benefits and ensures the delivery of the proposed work.
</p>
<p>
The term "include your business in the international market" means to explore and enjoy the productive capacity scale, predominant feature in China to reduce costs and risks in the operation. Another important point is that Coimaq gives customers access to innovations, new technologies and designs from various companies around the world. Many vendors develop innovative products and COIMAQ can share all this with its rich company portfolio.
</p>
STR;


$lang['SERVIÇOS Titulo']['pesquisa'] = "What COIMAQ can do for your business in the international market?";
$lang['SERVIÇOS Texto']['pesquisa'] = <<<STR
<img src='_imgs/internas/servicos_sub2.jpg'>
<p>
Searches can be performed in any region in China or neighboring countries whose commercial terms and product meet the expectations of our customers. COIMAQ performs searching in different regions and in different suppliers in order to guarantee our customers options for decision-making and better prices, without being hostage to a single vendor.
</p>
<p>
The search and selection process for suppliers considerate some important parameters defined at the beginning of each search. The result is a document with potential suppliers and qualified scorers, including photos, certificates and auxiliary related information, so that each client can select the best product and the best supplier.
</p>
<h3>
	The method may include the following steps:
</h3>
<ul>
	<li>Research and evaluation of suppliers;</li>
	<li>Factory Audits;</li>
	<li>Reviews and sampling of products;</li>
	<li>Laboratory evaluation;</li>
	<li>Collection of specific and needed test certificates.</li>
</ul>
STR;


$lang['SERVIÇOS Titulo']['desenvolvimento'] = "Want to develop a product with your own brand? Does your product have specific needs?";
$lang['SERVIÇOS Texto']['desenvolvimento'] = <<<STR
<img src='_imgs/internas/servicos_sub3.jpg'>
<p>
COIMAQ provides consultation for companies wishing to outsource the manufacturing process and product development, designs and trademarks in China. If there is no similar, COIMAQ can develop the design, drawings, packaging, schedule, and all the steps leading up to the selection of suppliers for production.
</p>
<p>
When the client has already approved the project, we format a proposal that starts in understanding the product; reviewing the prerequisites, quality reports, approval, negotiation, and delivery.
</p>
<h3>Complete Process</h3>
<img src='_imgs/internas/servicos_processo.png'>
<ol>
	<li><span class='destaque'>1</span>. Project Technical Design;</li>
	<li><span class='destaque'>2</span>. Suppliers Enquires;</li>
	<li><span class='destaque'>3</span>. Factory Audit;</li>
	<li><span class='destaque'>4</span>. Samples Approval;</li>
	<li><span class='destaque'>5</span>. Contracting;</li>
	<li><span class='destaque'>6</span>. Orders Management;</li>
	<li><span class='destaque'>7</span>. Quality Inspection;</li>
	<li><span class='destaque'>8</span>. Production Inspection;</li>
	<li><span class='destaque'>9</span>. Logistics Management;</li>
</ol>
<p>
According to the needs of each company the project could suffer adjustments. The process may also include the development and manufacture of custom packaging.
</p>
STR;


$lang['SERVIÇOS Titulo']['qualidade'] = "What COIMAQ can do for your business in the international market?";
$lang['SERVIÇOS Texto']['qualidade'] = <<<STR
<img src='_imgs/internas/servicos_sub4.jpg'>
<img src='_imgs/internas/passos123.jpg' style='margin-top:0;'>
<h3>
	Initial Inspection of Production
</h3>
<p>
The purpose of this inspection is to detect and prevent problems in order to avoid possible failures.  Some specifications and requirements of the product are addressed to be able to lead to an efficient result, achieving all product necessary functionalities and quality. This inspection includes the following steps:
</p>
<ul>
	<li>Evaluation of production lines and productive capacity of the supplier;</li>
	<li>Evaluation of equipment and technology used from factory;</li>
	<li>Revision of raw materials, key components and accessories used for production of goods;</li>
	<li>Revision of some semi-finished products and raw materials involved in production line;</li>
	<li>Checkup of inventory and supplier storage facility;</li>
	<li>Packaging  process and its evaluation.</li>
</ul>
<h3>
	Inspection During Production
</h3>
<p>
This inspection is performed when at least 20% or 30% of the production is started. Each step of the process is examined, including visual inspection of products.
</p>
<p>
Is during this process that COIMAW will compare the finished products achieved from supplier with samples previously approved by our client, and new samples can be sent for final approval of the customer.
</p>
<h3>
	Pre-shipment Inspection
</h3>
<p>
This inspection is performed before the goods are loaded and shipped and includes inspections randomly or uniformly depending on the type of product. Will be evaluated packaging and container conditions, in order to assure the safe transit of goods to destination.
</p>
<p>
	For this step, mainly check ups are as follows:
</p>
<ul>
	<li>Shipping Documentation;</li>
	<li>Quantities finally loaded;</li>
	<li>Dimensions of packages and their weight;</li>
	<li>Packaging and container conditions;</li>
	<li>Quality test according product requirements;</li>
	<li>Seals.</li>
<ul>
<p>
For certain products (e.g., raw materials and chemicals) COIMAQ may perform the specific laboratory tests. The reports will be sent to our customers in their preferred language.
</p>
STR;


$lang['SERVIÇOS Titulo']['logistica'] = "What COIMAQ can do for your business in the international market?";
$lang['SERVIÇOS Texto']['logistica'] = <<<STR
<img src='_imgs/internas/servicos_sub5.jpg'>
<p>
COIMAQ offers complete logistics solutions, including coordination of international transport of any modal (air, sea, road and rail) and other additional services such as insurance freight, consolidation, warehousing and distribution.
</p>
<p>	
With an extensive network of logistics partners scattered around the world and qualified employees to perform complex logistics operations. COIMAQ always searches for excellence in the management of the supply chain of our customers. Excellence in managing this process will reduce the lead-time and hence inventory costs and transportation.
</p>
<h3>
	Services
</h3>
<ul>
	<li>Air Shipping;</li>
	<li>Maritime Shipping;</li>
	<li>NVOCC Services;</li>
	<li>FCL Shipments;</li>
	<li>Cargo Consolidation / LCL;</li>
	<li>Services "door to door";</li>
	<li>RO RO cargo.</li>
</ul>
STR;


/**************************
    Prefixo : IMPORTAÇÃO
 	TEXTOS DA SEÇÃO IMPORTAÇÃO
***************************/
$lang['IMPORTAÇÃO Titulo'] = "International Trade with professionalism and transparency.";
$lang['IMPORTAÇÃO Já importa?'] = "YOUR COMPANY ALREADY IMPORTS?";
$lang['IMPORTAÇÃO Sim'] = "Yes";
$lang['IMPORTAÇÃO Não'] = "No";
$lang['IMPORTAÇÃO Importante Saber Titulo'] = "IMPORTANTE SABER!";
$lang['IMPORTAÇÃO Importante Saber Texto'] = <<<STR
<p>
China today has a lot of potential manufacturing and very aggressive priced compared to other countries. This is achieved primarily by encouraging production within the country (driven by the large amount of labor) and low tax burdens which industries are subjected.
</p>	
<p>
With approximately 9.6 million square kilometers and a population of 1.339 billion people, the People's Republic of China, today is undoubtedly the world's factory. With a nominal GDP of about 7.3 trillion dollars (2011 data), makes of China a country that continues to rise.
</p>
STR;
$lang['IMPORTAÇÃO Texto não'] = <<<STR
<p>
Our proposal is to develop a commercial structure in China, managing the procurement processes of goods ("procurement"), production, quality control and international logistics.
</p>
<p>
 Through a solid and active participation, COIMAQ can help your company reduce costs substantially when purchasing products on the international market, regardless of segment, always seeking safety and continuous improvement process.
</p>
<p>
 The products will be exported by COIMAQ directly to the importer, avoiding over-taxing, eliminating other intermediaries and additional expenses.
</p>
STR;
$lang['IMPORTAÇÃO Texto sim'] = <<<STR
<p>
We perform all services related to foreign trade process, from sourcing products to searching potential suppliers, development of special projects, quality inspections, cargo consolidation, exporting, local freight and international logistics.
</p>
STR;


/**************************
    Prefixo : CONTATO
 	TEXTOS DA SEÇÃO CONTATO
***************************/
$lang['CONTATO Título'] = "For any questions, suggestions or other information, please do not hesitate in contact us.";
$lang['CONTATO Titulo Formulario'] = "Envie-nos uma mensagem através do formulário"; /**/
$lang['CONTATO Nome'] = "Name";
$lang['CONTATO Nome da Empresa'] = "Company";
$lang['CONTATO E-mail'] = "E-mail";
$lang['CONTATO Telefone'] = "Phone";
$lang['CONTATO Mensagem'] = "Message";
$lang['CONTATO ENVIAR'] = "SEND";
$lang['CONTATO Resposta de Contato'] = "Obrigado por entrar em contato.<br>Responderemos assim que possível."; /**/

$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
/*
Cadastre-se para receber novidades, informativos e convites de eventos
Cadastro efetuado com sucesso!
Envie-nos uma mensagem através do formulário
Obrigado por entrar em contato. Responderemos assim que possível.
*/
?>