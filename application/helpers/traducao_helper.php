<?php

function traduz($label, $segun_arg = FALSE, $ajax = FALSE){
    
    $CI =& get_instance();

    if($ajax)
        $CI->lang->load($CI->session->userdata('language').'_site', $CI->session->userdata('language'));
    
    $return = $CI->lang->line($label);
    

    if($return)
        if($segun_arg){
            return $return[$segun_arg];
        }else{
            return $return;
        }
    else{
        log_message('debug', "Termo não encontrado : $label | Linguagem : ".$CI->session->userdata('language'));
        return $label;
    }
}

/*
 * Função para adicionar o prefixo definido na sessão de acordo com a linguagem
 */
function prefixo($arg){
    $CI =& get_instance();
    return $CI->session->userdata('prefixo').$arg;
}

?>