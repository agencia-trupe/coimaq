<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Importacao extends MY_Frontcontroller {

   	function __construct(){
   		parent::__construct();
   	}

   	function index($slug = false){
   		$data['slug'] = $slug;
   		$this->load->view('importacao', $data);
   	}

}