<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
   		$this->load->view('contato');
    }

    function enviar(){
   		$emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        /*
        $emailconf['protocol'] = 'smtp';
        $emailconf['smtp_host'] = 'smtp.robertomigotto.com.br';
        $emailconf['smtp_user'] = 'noreply@robertomigotto.com.br';
        $emailconf['smtp_pass'] = 'noreplymigotto';
        */
        $emailconf['crlf'] = "\r\n";
        $emailconf['newline'] = "\r\n";        

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $u_nome = $this->input->post('nome');
        $u_empresa = $this->input->post('nome-empresa');
        $u_email = $this->input->post('email');
        $u_telefone = $this->input->post('telefone');
        $u_msg = $this->input->post('mensagem');


        $from = 'contato@coimaq.com';
        $fromname = 'Contato';

        $to = 'contato@coimaq.com';
        $bc = FALSE;
        $bcc = 'bruno@trupe.net';

        $assunto = 'Contato via Site';
        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:14px;
                                color:#5DB8C9;
                                font-family:Arial;
                            }
                            .val{
                                color:#000;
                                font-size:12px;
                                font-family:Arial;
                            }
                        </style>
                    </head>
                    <body>
                    <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br />
                    <span class='tit'>Nome da Empresa :</span> <span class='val'>$u_empresa</span><br />
                    <span class='tit'>Telefone :</span> <span class='val'>$u_telefone</span><br />
                    <span class='tit'>Email :</span> <span class='val'>$u_email</span><br />
                    <span class='tit'>Mensagem :</span> <span class='val'>$u_msg</span>
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);

        if($bc)
            $this->email->cc($bc);
        if($bcc)
            $this->email->bcc($bcc);

        $this->email->reply_to($u_email);

        $this->email->subject($assunto);
        $this->email->message($email);

	    $this->email->send();

        $this->session->set_flashdata('envio', TRUE);
        redirect('index.php/contato');
    }    

}