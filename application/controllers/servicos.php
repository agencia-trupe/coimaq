<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicos extends MY_Frontcontroller {

   	function __construct(){
   		parent::__construct();
   	}

   	function index($slug = false){

   		$arrslugs = array(
   			'importacao',
   			'pesquisa',
   			'desenvolvimento',
   			'qualidade',
   			'logistica'
   		);

   		if(!$slug || !in_array($slug, $arrslugs))
   			redirect('servicos/importacao');

   		$data['slug'] = $slug;

   		$this->load->view('servicos', $data);
   	}

}