<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Download extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
    }
 
    public function index()
    {
        $this->load->dbutil();
 
        $users = $this->db->select('nome, email')->get('newsletter');
 
        $delimiter = ",";
        $newline = "\r\n";
 
        $this->output->set_header('Content-Type: application/force-download');
        $this->output->set_header('Content-Disposition: attachment; filename="cadastros_'.Date('d-m-Y_H-i-s').'.csv"');
        $this->output->set_content_type('text/csv')
                ->set_output($this->dbutil->csv_from_result($users, $delimiter, $newline));
    }
}