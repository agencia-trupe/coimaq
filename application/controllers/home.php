<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$this->load->view('home');
    }

    function cadastroNews(){
    	$nome = $this->input->post('nome');
    	$email = $this->input->post('email');

    	if($this->db->get_where('newsletter', array('email' => $email))->num_rows() == 0){
	    	$this->db->set('nome', $nome)
	    			 ->set('email', $email)
	    			 ->insert('newsletter');
		}
    }

}