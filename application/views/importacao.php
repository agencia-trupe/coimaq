<div id="h1-container">
	<h1><?=traduz('IMPORTAÇÃO Titulo')?></h1>
</div>

<div class="centro">

	<div class="coluna-lateral">

		<h2><?=traduz('IMPORTAÇÃO Já importa?')?></h2>

		<ul>
			<li class="negativo"><a href="index.php/importacao/nao" id="opcao-nao" title="<?=traduz('IMPORTAÇÃO Não')?>"><?=traduz('IMPORTAÇÃO Não')?></a></li>
			<li><a href="index.php/importacao/sim" id="opcao-sim" title="<?=traduz('IMPORTAÇÃO Sim')?>"><?=traduz('IMPORTAÇÃO Sim')?></a></li>
		</ul>

		<div id="importante-saber">
			<h3><?=traduz('IMPORTAÇÃO Importante Saber Titulo')?></h3>
			<?=traduz('IMPORTAÇÃO Importante Saber Texto')?>

		</div>
	</div><!--

	--><div class="coluna-direita">

		<img src="_imgs/internas/importacao.jpg">

		<div class="nao box">
			<?=traduz('IMPORTAÇÃO Texto não')?>
		</div>

		<div class="sim box">
			<?=traduz('IMPORTAÇÃO Texto sim')?>
		</div>

	</div>

</div>

<?php if (isset($slug) && $slug && ($slug == 'sim' || $slug == 'nao')): ?>
	<script defer>
	$('document').ready( function(){
		setTimeout(function(){
			$('#opcao-<?=$slug?>').trigger('click');
		}, 150);
	});
	</script>
<?php endif ?>