<div id="h1-container">
	<h1><?=traduz('CONTATO Título')?></h1>
</div>

<div class="centro">

	<div class="coluna-lateral">
		
		<a href="mailto:contato@coimaq.com" class="mail-link" title="<?=traduz('RDP ENTRE EM CONTATO')?>">contato@coimaq.com</a>

		<div class="box-telefone brasil">
			<h2><?=traduz('BRASIL - SÃO PAULO')?></h2>
			+55 (11) 4063 1660
		</div>

		<div class="box-telefone china">
			<h2><?=traduz('CHINA - XANGAI')?></h2>
			+86 (021) 6095 0622
		</div>

		<img src="_imgs/internas/contato.jpg">

	</div><!--

	--><div class="coluna-direita">

		<h4><?=traduz('CONTATO Titulo Formulario')?>:</h4>

		<?php if ($this->session->flashdata('envio')): ?>

			<div class="resposta">
				<?=traduz('CONTATO Resposta de Contato')?>
			</div>

		<?php else: ?>	

			<form id="form-contato" method="post" action="index.php/contato/enviar">

				<input type="text" name="nome" placeholder="<?=traduz('CONTATO Nome')?>" required>

				<input type="text" name="nome-empresa" placeholder="<?=traduz('CONTATO Nome da Empresa')?>" required>

				<input type="email" name="email" placeholder="<?=traduz('CONTATO E-mail')?>" required>

				<input type="text" name="telefone" placeholder="<?=traduz('CONTATO Telefone')?>" required>

				<textarea name="mensagem" placeholder="<?=traduz('CONTATO Mensagem')?>" required></textarea>

				<div id="submit-container">
					<input type="submit" value="<?=traduz('CONTATO ENVIAR')?> &raquo;">
				</div>

			</form>

		<?php endif ?>

	</div>

</div>