<header>

	<div class="centro">

		<a href="<?=base_url()?>" title="<?=traduz('ATIT Página Inicial')?>" id="home-link"><img src="_imgs/layout/logo.png" alt="<?=CLIENTE?>"></a>

		<div id="barra-superior">
			<div class="telefone">
				+55 (11) 4063 1660 | +86 (021) 6095 0622
			</div><!--
			--><div class="linguagem">
				<a id="lan-pt" href="index.php/linguagem/index/pt" title="versão em português"><img src="_imgs/layout/br.png" alt="Português"></a>
				<a id="lan-en" href="index.php/linguagem/index/en" title="english version"><img src="_imgs/layout/us.png" alt="English"></a>
				<!-- 
				<a id="lan-es" href="index.php/linguagem/index/es" title="versión en español"><img src="_imgs/layout/es.png" alt="Español"></a>
				<a id="lan-ch" href="index.php/linguagem/index/ch" title="中国版"><img src="_imgs/layout/ch.png" alt="中国的"></a>
				-->
			</div>
		</div>

		<nav>
			<ul>
				<li>
					<a href="index.php/home" title="<?=traduz('ATIT Página Inicial')?>" id="mn-home" <?if($this->router->class=='home')echo" class='ativo'"?>>
						<?=traduz('MN Home')?>
					</a>
				</li>
				<li>
					<a href="index.php/empresa" title="<?=traduz('ATIT Empresa')?>" id="mn-empresa" <?if($this->router->class=='empresa')echo" class='ativo'"?>>
						<?=traduz('MN Empresa')?>
					</a>
				</li>
				<li>
					<a href="index.php/servicos/importacao" title="<?=traduz('ATIT Serviços')?>" id="mn-servicos" <?if($this->router->class=='servicos')echo" class='ativo'"?>>
						<?=traduz('MN Serviços')?>
					</a>
				</li>
				<li>
					<a href="index.php/importacao" title="<?=traduz('ATIT Importação')?>" id="mn-importacao" <?if($this->router->class=='importacao')echo" class='ativo'"?>>
						<?=traduz('MN Importação')?>
					</a>
				</li>
				<li>
					<a href="index.php/contato" title="<?=traduz('ATIT Contato')?>" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>
						<?=traduz('MN Contato')?>
					</a>
				</li>
			</ul>		
		</nav>

	</div>
	
</header>

<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">