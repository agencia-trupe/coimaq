
  </div> <!-- fim da div main -->

  <footer>

    <div class="superior">

      <div class="centro">

        <div class="coluna">
          <ul>
            <li>&raquo;&nbsp;<a href="index.php/home" title="<?=traduz('ATIT Página Inicial')?>"><?=traduz('MN Home')?></a></li>
            <li>&raquo;&nbsp;<a href="index.php/empresa" title="<?=traduz('ATIT Empresa')?>"><?=traduz('MN Empresa')?></a></li>
            <li>&raquo;&nbsp;<a href="index.php/servicos/importacao" title="<?=traduz('ATIT Serviços')?>"><?=traduz('MN Serviços')?></a></li>
          </ul>
        </div>
        <div class="coluna">
          <ul>
            <li>&raquo;&nbsp;<a href="index.php/importacao" title="<?=traduz('ATIT Importação')?>"><?=traduz('MN Importação')?></a></li>
            <li>&raquo;&nbsp;<a href="index.php/contato" title="<?=traduz('ATIT Contato')?>"><?=traduz('MN Contato')?></a></li>
          </ul>
        </div>

        <div id="box-container">
          <div class="box telefone brasil">
            <strong>BRASIL - SÃO PAULO</strong><br>
            +55 (11) 4063 1660
          </div>
          <div class="box telefone china">
            <strong>CHINA - SHANGHAI</strong><br>
            +86 (021) 6095 0622
          </div>
          <div class="box email">
            <strong><?=traduz('RDP ENTRE EM CONTATO')?></strong><br>
            <a href="mailto:contato@coimaq.com" title="<?=traduz('RDP ENTRE EM CONTATO')?>">contato@coimaq.com</a>
          </div>
        </div>

      </div>

    </div>

    <div class="inferior">
      <div class="centro">
        &copy; <?=Date('Y')?> COIMAQ - <?=traduz('RDP Todos os direitos reservados')?>
        <a href="http://www.trupe.net" title="Criação de sites : Trupe Agência Criativa" target="_blank"><span>Criação de sites : Trupe Agência Criativa</span><img src="_imgs/layout/trupe.png" alt="Trupe Agência Criativa"></a>
      </div>
    </div>
  
  </footer>
  
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-37681819-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>

  <?JS(array('cycle', 'front'))?>
  
</body>
</html>
