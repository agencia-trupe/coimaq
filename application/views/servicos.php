<div id="h1-container">
	<h1><?=traduz('SERVIÇOS Titulo', $slug)?></h1>
</div>

<div class="centro">

	<div class="coluna-lateral">
		<ul>
			<li><a href="index.php/servicos/importacao" class="<?if($slug=='importacao')echo'ativo'?>" title="<?=traduz('SUB Importação de Produtos da China')?>"><?=traduz('SUB Importação de Produtos da China')?></a></li>
			<li><a href="index.php/servicos/pesquisa" class="<?if($slug=='pesquisa')echo'ativo'?>" title="<?=traduz('SUB Pesquisa de Produtos e Fornecedores')?>"><?=traduz('SUB Pesquisa de Produtos e Fornecedores')?></a></li>
			<li><a href="index.php/servicos/desenvolvimento" class="<?if($slug=='desenvolvimento')echo'ativo'?>" title="<?=traduz('SUB Desenvolvimento de Produtos')?>"><?=traduz('SUB Desenvolvimento de Produtos')?></a></li>
			<li><a href="index.php/servicos/qualidade" class="<?if($slug=='qualidade')echo'ativo'?>" title="<?=traduz('SUB Inspeção de Qualidade')?>"><?=traduz('SUB Inspeção de Qualidade')?></a></li>
			<li><a href="index.php/servicos/logistica" class="<?if($slug=='logistica')echo'ativo'?>" title="<?=traduz('SUB Logística Internacional')?>"><?=traduz('SUB Logística Internacional')?></a></li>
		</ul>		
	</div><!--

	--><div class="coluna-direita">

		<?=traduz('SERVIÇOS Texto', $slug)?>

	</div>

</div>