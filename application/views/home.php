<div class="centro">

	<div class="coluna-lateral">

		<h1><?=traduz('HOME NOSSO PROCESSO')?></h1>

		<ul>
			<li><a href="index.php/servicos/importacao" title="<?=traduz('SUB Importação de Produtos da China')?>"><?=traduz('SUB Importação de Produtos da China')?></a></li>
			<li><a href="index.php/servicos/pesquisa" title="<?=traduz('SUB Pesquisa de Produtos e Fornecedores')?>"><?=traduz('SUB Pesquisa de Produtos e Fornecedores')?></a></li>
			<li><a href="index.php/servicos/desenvolvimento" title="<?=traduz('SUB Desenvolvimento de Produtos')?>"><?=traduz('SUB Desenvolvimento de Produtos')?></a></li>
			<li><a href="index.php/servicos/qualidade" title="<?=traduz('SUB Inspeção de Qualidade')?>"><?=traduz('SUB Inspeção de Qualidade')?></a></li>
			<li><a href="index.php/servicos/logistica" title="<?=traduz('SUB Logística Internacional')?>"><?=traduz('SUB Logística Internacional')?></a></li>
		</ul>

	</div><!--

	--><div id="slideshow">

		<div id="navegacao"></div>

		<div id="animate">

			<div class="slide">
				<img src="_imgs/home/home_banner1.jpg">
				<div class="texto">
					<?=traduz('HOME Texto Slide 1')?>				
				</div>
			</div>

			<div class="slide" style="display:none;">
				<img src="_imgs/home/home_banner2.jpg">
				<div class="texto">
					<?=traduz('HOME Texto Slide 2')?>				
				</div>
			</div>

			<div class="slide" style="display:none;">
				<img src="_imgs/home/home_banner3.jpg">
				<div class="texto">
					<?=traduz('HOME Texto Slide 3')?>				
				</div>
			</div>

		</div>

	</div>

	<h1>
		<?=traduz('HOME FERIAS E EVENTOS INTERNACIONAIS')?>
	</h1>

	<div class="coluna-lateral">
		<img src="_imgs/layout/canton-fair.png" alt="Canton Fair">
		<div class="box-cinza">
			<?=traduz('HOME Chamada Viagens')?>
		</div>
		<a href="index.php/" class="link-destaque" title="<?=traduz('HOME PROGRAME SUA VIAGEM')?>"><?=traduz('HOME PROGRAME SUA VIAGEM')?>&nbsp;&raquo;</a>
	</div><!--

	--><div class="coluna-direita">
		<img src="_imgs/home/home_img1.jpg"><img src="_imgs/home/home_verde-amarelo.png"><img src="_imgs/home/home_img2.jpg">
	</div>

	<div id="faixa">
		<?=traduz('HOME Texto Faixa')?>		
	</div>

	<div id="chamadas-home-container">

		<div class="coluna">
			<h2 class="goog"><?=traduz('HOME Chamada Inferior 1 Titulo')?></h2>
			<p>
				<?=traduz('HOME Chamada Inferior 1 Texto')?>
			</p>
			<a href="index.php/servicos/pesquisa" title="<?=traduz('saiba mais')?>"><?=traduz('saiba mais')?> &raquo;</a>
		</div><!--

		--><div class="coluna">
			<h2 class="check"><?=traduz('HOME Chamada Inferior 2 Titulo')?></h2>
			<p>
				<?=traduz('HOME Chamada Inferior 2 Texto')?>
			</p>
			<a href="index.php/servicos/qualidade" title="<?=traduz('saiba mais')?>"><?=traduz('saiba mais')?> &raquo;</a>
		</div><!--

		--><div class="coluna ultima">
			<h2 class="cog"><?=traduz('HOME Chamada Inferior 3 Titulo')?></h2>
			<p>
				<?=traduz('HOME Chamada Inferior 3 Texto')?>
			</p>
			<a href="index.php/servicos/desenvolvimento" title="<?=traduz('saiba mais')?>"><?=traduz('saiba mais')?> &raquo;</a>
		</div>

	</div>

	<div id="barra-newsletter">

		<span class="titulo"><?=traduz('HOME Texto Newsletter')?>:</span>

		<form id="form-newsletter" method="post" action="index.php/home/cadastroNews">
			<input type="text" name="nome" id="newsletter-nome" placeholder="<?=traduz('Nome')?>">
			<input type="email" name="email" id="newsletter-email" placeholder="<?=traduz('E-mail')?>">
			<input type="submit" value="<?=traduz('ENVIAR')?> &raquo;">
		</form>

		<div id="mensagem-newsletter" class="hid">
			<?=traduz('Retorno Newsletter')?>
		</div>

	</div>

</div>